/*
 * File:   GeneralAllocator.cpp
 * Author: hammy
 *
 * Created on Jul 02, 2022 at 12:33 AM
 */

#include "lightsky/utils/GeneralAllocator.hpp"



template class ls::utils::GeneralAllocator<sizeof(unsigned long long)*4>;
